import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { PositionReportTuple } from 'src/app/autenticacao/model/PositionReportTuple';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.css'],
})
export class PositionComponent implements OnInit {
  displayedColumns: string[] = ['demo-name', 'demo-symbol'];
  data: PositionReportTuple[] = [];

  constructor(
    private adminService: AdminService,
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  private getData() {
    this.adminService.getPositionReport().subscribe({
      next: (response: any) => {
        this.data = response.body;
        console.log(response);
      },
      error: () => {
        this.adminService.openSnackBar('Não foi possível carregar os dados.', 'red-snackbar');
      }
    });
  }
}
