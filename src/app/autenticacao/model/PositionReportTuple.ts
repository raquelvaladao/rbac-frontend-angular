export interface PositionReportTuple {
    position: string;
    quantity: number;
}